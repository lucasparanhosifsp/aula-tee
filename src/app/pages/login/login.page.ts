import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  Usuario: string; Senha: string; Erro: string; TemErro: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  Login() {
    if (this.Usuario === "Lucas" && this.Senha === "123456") {
      this.router.navigateByUrl('/home');
      this.TemErro = false;
    } else {
      this.Erro = "Usuário ou senha incorretos";
      this.TemErro = true;
    }
  }

}
